<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'PageController@home')->name('home');

Route::get('/users/{npm}', function($npm) {
	return "Cari user by NPM " . $npm;
})->where(['npm' => '[0-9]+'])->name('getUserByNPM');

Route::get('/users/{nama}', function($nama) {
	return "Cari user by nama " . $nama;
});

use App\User;

Route::get('test', function () {
	return "<a href='".
		route('getUserByNPM', '1406565133').
			"'>link</a>";
});

Route::group([
	'prefix' => 'admin',
	'namespace' => 'Admin'
], function () {
	Route::get('/', function () { 
		return "dashboard admin"; 
	});

	Route::get('/users', function (){
		return "daftar user";
	});

	Route::get('/lala', 'AdminController@lala');

	Route::get('home', 'AdminController@home');
});

Route::resource('photos', 'PhotoController');

Route::get('magic', 'PageController@magic');