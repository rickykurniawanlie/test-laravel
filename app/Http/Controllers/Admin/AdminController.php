<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use \App\Http\Controllers\Controller as Controller;
use Route;

class AdminController extends Controller
{
    public function lala() {
    	return response()->json(Route::current());
    }

    public function home() {
    	return view('admin.home');
    }
}
